package com.devcamp.s10;

import java.util.Arrays;

public class Person {
    // các thuộc tính (properties)
    String name;
    int age;
    double weight;
    long salary;
    String[] pets;
    
    // các phương thức tạo (constructors)
    public Person() {
        this.name = "Defaut";
        this.age = 1;
        this.weight = 5.5d;
        this.salary = 10000000l;
    }

    public Person(String paramName) {
        this.name = paramName;
    }

    public Person(String paramName, int paramAge, double paramWeight) {
        this.name = paramName;
        this.age = paramAge;
        this.weight = paramWeight;
    }

    public Person(String paramName, int paramAge, double paramWeight, long paramSalary, String[] paramPets) {
        this.name = paramName;
        this.age = paramAge;
        this.weight = paramWeight;
        this.salary = paramSalary;
        this.pets = paramPets;
    }

    // phuong thuc
    public void showPersonInfor() {
        System.out.println("Họ tên: " + this.name);
        System.out.println("Tuổi: " + this.age);
        System.out.println("Cân nặng: " + this.weight);
    }

    @Override
    public String toString() {
        String personInfor = "";
        personInfor = "Họ tên: " + this.name + ", ";
        personInfor += "Tuổi: " + this.age + ", ";
        personInfor += "Cân nặng: " + this.weight + ", ";
        personInfor += "Lương: " + this.salary + ",";
        personInfor += "Thú cưng: " + Arrays.toString(this.pets);
        return personInfor;
    }
}
