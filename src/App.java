import java.util.ArrayList;

import com.devcamp.s10.Person;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Person> arrPersons = new ArrayList<>();
        String[] arrPets = {"Cat", "Dog", "Bird"};
        // khởi tạo 4 đối tượng
        Person person1 = new Person();
        Person person2 = new Person("Dinh Linh");
        Person person3 = new Person("Ngo Long", 34, 60.5d);
        Person person4 = new Person("Phan Bich Tram", 25, 54d, 25000000l, arrPets);

        // add các person vào mảng
        arrPersons.add(person1);
        arrPersons.add(person2);
        arrPersons.add(person3);
        arrPersons.add(person4);

        for (Person personObj : arrPersons) {
            System.out.println(personObj);
        }
    }
}
